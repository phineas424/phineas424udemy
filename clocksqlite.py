
import pyodbc
import adodbapi
import sqlite3
from tkinter import *
from datetime import datetime
#from time import time
import time
import sys


winMain = Tk()
winMain.wm_title("Time Clock App")
winMain.minsize(width=350, height=300)

def connect():
    conn=sqlite3.connect("timeclock.db")
    cur=conn.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS ClockUsers(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, lastname TEXT, pass TEXT, active INTEGER)")
    cur.execute("CREATE TABLE IF NOT EXISTS TimeHistory(id INTEGER PRIMARY KEY AUTOINCREMENT, userid INTEGER, punchtime DATETIME)")
    conn.commit()
    conn.close()

def listusers():
    # DB CONNECTION
    conn=sqlite3.connect("timeclock.db")
    cur=conn.cursor()
    qrystr="SELECT id, name, lastname, pass  FROM ClockUsers WHERE active=-1 ORDER BY lastname DESC, name DESC"
    cur.execute(qrystr)
    rows=cur.fetchall()
    conn.close()
    return rows

def createuser(n,ln,pas,act):
    conn=sqlite3.connect("timeclock.db")
    cur=conn.cursor()
    cur.execute("INSERT INTO ClockUsers VALUES (NULL,?,?,?,?)",(n, ln, pas, act))
    conn.commit()
    conn.close()

connect()

#createuser('philip','derossi','0000',-1)
#createuser('nina','jacobs','2222',-1)
#createuser('rosa','vargas','1111',-1)
#createuser('sam','jacobs','4444',-1)
#createuser('jeff','pardes','5555',-1)
#createuser('samuel','minond','6666',-1)

users=listusers()
for user in users:
    print(user)

#Login form
global userentry_text
global pass_text
userentry_text=StringVar()
pass_text=StringVar()

# FORM ELEMENTS DEFINITIONS
#  labels, entry fields
l1=Label(winMain,text="select user")
list1entry=Entry(winMain,textvariable=userentry_text,relief=RIDGE, bg="#c7ccf9",fg="purple",font="Arial 8 bold", width=20)
l2=Label(winMain,text="password")
e2=Entry(winMain,textvariable=pass_text,relief=RIDGE,bg="#c7ccf9",fg="purple",font="Arial 8 bold", width=10, show="*")

#   users listbox
list1=Listbox(winMain,height=6,width=20)
sb1=Scrollbar(winMain)
list1.configure(yscrollcommand=sb1.set)
sb1.configure(command=list1.yview)
list1.delete(0, END) #deletes the current value
for user in users: #populate listbox
    list1.insert(0,(user[2]+", "+user[1])) #inserts new value assigned

def get_selected_row(event):
    try:
        global selected_tuple
    #    global selected_id
        index=list1.curselection()[0]
        selected_tuple=list1.get(index)
        print(selected_tuple)
        list1entry.delete(0, END) #deletes the current value
        list1entry.insert(0, selected_tuple) #inserts new value assigned
    except IndexError:
        pass

list1.bind('<<ListboxSelect>>', get_selected_row)

def login_command():
    # DB CONNECTION
    conn=sqlite3.connect("timeclock.db")
    cur=conn.cursor()
    myuser=str(userentry_text.get()).strip()
    mypass=str(pass_text.get()).strip()
    qrystr="SELECT id, name, lastname, pass  FROM ClockUsers WHERE active=-1 AND pass='"+mypass+"' AND (trim(lastname) || ', ' || trim(name))='"+myuser+"'"
    cur.execute(qrystr)
    result=cur.fetchone()

    log_success=0
    if result!=None:
        log_success=1
        myid=result[0]
        print("result is %s " % (result,))
        print("result id is %s " % (myid,))

    if log_success==0:
        print("login failed - incorrect password for user '%s'" % (myuser,))
    elif log_success==1:
        print("login success for user '%s'" % (myuser,))
        display_timehistory(myid, myuser)

    conn.close()

b1=Button(winMain,text="Login", width=20,command=login_command)
b2=Button(winMain,text="Close",width=12,command=winMain.destroy)

def display_timehistory(thisid, thisuser):
    winHistory = Tk()
    winHistory.wm_title(thisuser+" - Time Clock History")
    winHistory.minsize(width=350, height=300)
    lh1=Label(winHistory,text="current time")
    lh1.grid(row=1,column=1)
    global thisid2
    thisid2=thisid
    time = ''
    clockh = Label(winHistory, font=('arial', 8, 'normal'), bg='lightyellow', fg='red')
    clockh.grid(row=2,column=1)

    def tick():
        global time
        time=datetime.now()
        time2 = time.strftime('%A, %Y-%m-%d %H:%M:%S')
        if time2 != time:
            time = time2
            clockh.config(text=time2)
        clockh.after(200, tick)

    tick()

    def savepunch_command():
        # DB CONNECTION
        conn=sqlite3.connect("timeclock.db")
        cur=conn.cursor()
        thisptime=datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')
        qrystr="INSERT INTO TimeHistory(userid, punchtime) VALUES (" +str(thisid2)+", '"+str(thisptime)+"')"
        print(qrystr)
        cur.execute(qrystr)
        conn.commit()
        conn.close()

        winHistory.destroy()

    # DB CONNECTION
    conn=sqlite3.connect("timeclock.db")
    cur=conn.cursor()
    qrystr="SELECT punchtime FROM TimeHistory WHERE userid="+str(thisid)+" ORDER BY punchtime DESC"
    cur.execute(qrystr)
    rows=cur.fetchall()
    conn.close()

    lh1=Label(winHistory,text="punch time history")
    lh1.grid(row=3,column=1)
    listh1=Listbox(winHistory,height=6,width=20)
    sbh1=Scrollbar(winHistory)
    listh1.configure(yscrollcommand=sbh1.set)
    sbh1.configure(command=listh1.yview)
    listh1.grid(row=4,column=1,rowspan=6,columnspan=1)
    sbh1.grid(row=4,column=2,rowspan=6)
    listh1.delete(0, END) #deletes the current value
    for row in rows: #populate listbox
        listh1.insert(0, row[0]) #inserts new value assigned

    bh1=Button(winHistory,text="Save Current Punch Time", width=24, command=savepunch_command)
    bh2=Button(winHistory,text="Close",width=12,command=winHistory.destroy)
    bh1.grid(row=11,column=1)
    bh2.grid(row=13,column=1)

    winHistory.mainloop()

# FORM GRID SETTINGS
#   labels, entry fields, list box, buttons
l1.grid(row=1,column=1)
l2.grid(row=10,column=1)
list1entry.grid(row=2,column=1)
list1.grid(row=3,column=1,rowspan=6,columnspan=1)
sb1.grid(row=3,column=2,rowspan=6)
e2.grid(row=11,column=1)
b1.grid(row=12,column=1)
b2.grid(row=14,column=4)

winMain.mainloop()
